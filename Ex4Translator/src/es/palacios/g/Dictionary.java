package es.palacios.g;

import java.util.HashMap;

public class Dictionary {

    private HashMap<String, String> content;

    public Dictionary() {
        loadContent();
    }

    public String getTranslation(String word) {
        if (content.containsKey(word)) {
            return content.get(word);
        }
        return null;
    }

    private void loadContent() {
        content = new HashMap<>();
        content.put("gato", "cat");
        content.put("perro", "dog");
        content.put("leon", "lion");
        content.put("tigre", "tiger");
        content.put("negro", "black");
        content.put("blanco", "white");
        content.put("amarillo", "yellow");
        content.put("verde", "green");
        content.put("rojo", "red");
        content.put("naranja", "orange");
        content.put("violeta", "purple");
        content.put("rosa", "pink");
        content.put("lapiz", "pencil");
        content.put("teclado", "keyboard");
        content.put("ordenador", "computer");
        content.put("raton", "mouse");
        content.put("monitor", "monitor");
        content.put("television", "tv");
        content.put("aparato", "device");
        content.put("electrodoméstico", "appliance");
    }

}

