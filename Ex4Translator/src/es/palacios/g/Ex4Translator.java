package es.palacios.g;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Ex4Translator {
    public static void main(String[] args) {
        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in))) {
            Dictionary dictionary = new Dictionary();
            String word;

            while ((word = bufReader.readLine()) != null && !word.equalsIgnoreCase("Finish")) {
                String translation;
                translation = dictionary.getTranslation(word);
                if (translation != null) {
                    System.out.println("Spanish: " + word + " - English: " + translation);
                } else {
                    System.out.println("Unknown");
                }
            }
            
        } catch (Exception e) {
            System.err.println("Exception in Ex4Translator Program: " + e.getMessage());
            System.err.println("Ex4Translator StackTrace ===>>");
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
