package es.palacios.g;

import java.io.*;

public class Ex4Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        final String ex4TranslatorJarPath = "out/artifacts/Ex4Translator_jar/Ex4Translator.jar";

        Process childProcess = new ProcessBuilder("java", "-jar", ex4TranslatorJarPath).start();

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(childProcess.getOutputStream());
        InputStreamReader inputStreamReader = new InputStreamReader(childProcess.getInputStream());

        try (BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
             BufferedReader bufReader = new BufferedReader(inputStreamReader)) {

            BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("=========== SPANISH - ENGLISH TRANSLATOR ==========");

            String line;
            boolean goOn;
            do {
                System.out.print("Write a word in Spanish to translate it into English (write 'finish' to Stop): ");
                line = bufReadCommLine.readLine();

                bufWriter.write(line);
                bufWriter.newLine();

                bufWriter.flush();

                goOn = !line.equalsIgnoreCase("finish");
                if (goOn) {
                    System.out.println(bufReader.readLine());
                }
            } while (goOn);

        } finally {
            int exitval = childProcess.waitFor();

            System.out.println("\nI finished translating from Spanish to English. Child process Exit value is: " + exitval);
            if (exitval != 0) {
                System.out.println(System.lineSeparator() + "ATTENTION: ERROR IN CHILD PROCESS FOUND -->");
                printChildErr(childProcess.getErrorStream());
            }
        }
    }

    private static void printChildErr(InputStream errorStream) {
        BufferedReader br = new BufferedReader(new InputStreamReader(errorStream));
        String line;
        try {
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }

    }

}
